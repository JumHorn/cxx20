import <iostream>;
using namespace std;

/*
likely already in linux source code is gnu extension
it is used to give compiler info to optimize code
*/

int main()
{
	int n = 40;
	[[likely]] if (n < 100) { cout << n * 2; }

	[[unlikely]] while (n > 100)
	{
		n = n / 2;
		cout << n << endl;
	}

	n = 500;
	[[likely]] if (n < 100) { cout << n * 2; }

	[[unlikely]] while (n > 100)
	{
		n = n / 2;
		cout << n << endl;
	}
	return 0;
}