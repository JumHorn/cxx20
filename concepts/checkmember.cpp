import <concepts>;
import <iostream>;
using namespace std;

template <typename T>
concept printable = requires(T t)
{
	t.print(); //must have print member function
};

template <typename T>
requires printable<T>
void log(T &t)
{
	t.print();
}

class Dog
{
public:
	void print1()
	{
		cout << "hello world" << endl;
	}
};

int main()
{
	Dog d;
	log(d);
	return 0;
}