import <concepts>;
import <iostream>;
import <type_traits>;
using namespace std;

template <typename T>
concept MustBeIntegerType = std::is_integral_v<T>;

// T must be integral
template <MustBeIntegerType T>
T add(T a, T b)
{
	return a + b;
}

int main()
{
	add(3, 5);
	return 0;
}