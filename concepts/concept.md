# concept

学习一个新概念要思考一下之前没有这个功能时，如何解决该问题
痛点在哪，现在是如何解决的

# 检查类型是否满足

1. C++20解决方案
```C++
template <typename T>
concept integral = std::is_integral_v<T>;
```
2. C++20之前的解决方案
```C++
// define enable_if_t stl source code
template <bool _Test, class _Ty = void>
struct enable_if {}; // no member "type" when !_Test

template <class _Ty>
struct enable_if<true, _Ty> { // type is _Ty for _Test
    using type = _Ty;
};

template <bool _Test, class _Ty = void>
using enable_if_t = typename enable_if<_Test, _Ty>::type;
// end of enable_if_t

// 添加add方法，参数类型确认是第二个模版参数确定的，并且是默认参数初始值是0(实际参数省略了)
//template <typename T, std::enable_if_t<std::is_integral_v<T>, T> param = 0 >
template <typename T, std::enable_if_t<std::is_integral_v<T>, T> = 0 >
T add(T a, T b) {
    return a + b;
}
```

# 检查对象是否有成员函数
1. C++20解决方案
```C++
template <typename T>
concept printable = requires(T t)
{
	t.print(); //must have print member function
};
```
2. C++20之前的解决方案

比较复杂，并没有列出

# 问题延伸
## 省略参数之默认参数
```C++
// 这个定义是正确的
int add(int a,int = 0);
```