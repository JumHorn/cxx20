/*
# 新概念学习方法

	学习concept时，我想到了这个例子
	并引用通用学习方法

1. 为什么要为编译器添加此功能(要解决的问题是什么)
2. 使用了什么样的解决方案(如何解决问题的)

	添加此功能之前代码是怎么实现的
	添加此功能之后代码是怎么实现的

## 三路比较运算符<=>

Three-way Comparison是C++20引入的

1. 为什么要为编译器添加此功能(问题是什么)

C++运算符重载==, !=, <, >, <=, >=
每次要实现6个函数，代码过于冗余

2. 使用了什么样的解决方案(如何解决问题的)

```C++
auto operator <=> (const <typename>&) const = default;
```
现在只需要定义以上两个，编译器自动生成==, !=, <, >, <=, >=
*/

import <compare>;
import <iostream>;
import <string>;
using namespace std;

template <typename T>
class DataType
{
private:
	T data;

public:
	//ctor
	DataType() : data(){};
	DataType(const T &val) : data(val){};

	auto operator<=>(const DataType<T> &other) const = default;
};

int main()
{
	DataType<int> a = 3, b{4};
	DataType<string> str1 = string("hello"), str2{"world"};
	if (a != b)
		cout << "a!=b" << endl;
	if (str1 == str2)
		cout << "str1==str2" << endl;
	return 0;
}