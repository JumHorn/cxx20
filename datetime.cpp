import <chrono>;
import <iostream>;
using namespace std;
using namespace std::chrono;

long fibonacci(unsigned n)
{
	if (n < 2)
		return n;
	return fibonacci(n - 1) + fibonacci(n - 2);
}

int main()
{
	// clock and time point
	// steady_clock like a stop watch
	// system_clock like a watch
	auto start = steady_clock::now();
	cout << "f(42) = " << fibonacci(42) << '\n';
	auto end = steady_clock::now();
	chrono::duration<double> elapsed_seconds = end - start;
	cout << "elapsed time: " << elapsed_seconds.count() << "s\n";

	time_point<system_clock> now = system_clock::now();

	// time of day
	// hh_mm_ss<duration> day;

	// Calendar
	//y,d Literals
	year_month_day date(2020y, July, 8d);
	date = chrono::floor<chrono::days>(now);
	cout << int(date.year()) << endl;
	cout << unsigned(date.month()) << endl;
	cout << unsigned(date.day()) << endl;

	// timezone not supported now
	// auto localnow = chrono::zoned_time{current_zone(), system_clock::now()}.get_local_time();
	// hh_mm_ss time{floor<milliseconds>(localnow - chrono::floor<chrono::days>(localnow))};

	hh_mm_ss time{floor<milliseconds>(now - chrono::floor<chrono::days>(now))};
	cout << time.hours().count() << endl;
	cout << time.minutes().count() << endl;
	cout << time.seconds().count() << endl;
	return 0;
}