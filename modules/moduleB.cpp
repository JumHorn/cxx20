//Global module fragment
module;
#include <cmath>
export module B;

export int modPow2(int a, int b)
{
	return (int)pow(2, a) % b;
}