export module A;

export int add(int a, int b)
{
	return a + b;
}

//Private module fragment
//sorry, unimplemented (g++11 20220501)
// module: private;
// int add(int a, int b);
// {
// 	return a + b;
// }