# CXX20

learning notes for c++20

# Getting started
## 编译器版本
> gcc11

1. Mac(big sur)
```shell
brew install gcc # default version is gcc11
g++-11 -v
```
2. Centos8
```shell
dnf -y install gcc-toolset-11-gcc gcc-toolset-11-gcc-c++
source /opt/rh/gcc-toolset-11/enable # make it useful for current bash
```

3. Debian 11(bullseye)
gcc10 already support c++20

4. Ubuntu
```shell
sudo add-apt-repository -y ppa:ubuntu-toolchain-r/test # add repo
sudo apt install -y g++-11 # install g++-11
update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-11 100 # set as defualt
```


## coroutine
## modules
## concept
## others
### three way comparison
### likely
### format
### calendar